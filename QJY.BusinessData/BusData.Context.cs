﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace QJY.BusinessData
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class QJ_OneLotusBIEntities : DbContext
    {
        public QJ_OneLotusBIEntities()
            : base("name=QJ_OneLotusBIEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<SZHL_TSSQ> SZHL_TSSQ { get; set; }
        public virtual DbSet<SZHL_GZGL> SZHL_GZGL { get; set; }
        public virtual DbSet<SZHL_GZGL_FL> SZHL_GZGL_FL { get; set; }
        public virtual DbSet<SZHL_GZGL_JCSZ> SZHL_GZGL_JCSZ { get; set; }
        public virtual DbSet<SZHL_GZGL_WXYJ> SZHL_GZGL_WXYJ { get; set; }
        public virtual DbSet<SZHL_XZ_GZD> SZHL_XZ_GZD { get; set; }
        public virtual DbSet<SZHL_XZ_JL> SZHL_XZ_JL { get; set; }
    }
}
